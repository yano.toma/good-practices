# Good Practices

Guía de buenas prácticas
- No es obligatorio de usar
- Es basado en la experiencia
- Siempre hay espacio para mejoras

## Readability > Optimization
Siempre hay que tratar de que nuestro código sea fácil de leer sin descuidar la optimización. Es mejor que nuestros compañeros entiendan lo que estoy programando y que en unos meses yo también lo siga entendiendo.

No todo debe estar escrito en una sola línea.

## Comments

- Cuándo agregar comentarios?
  - Cuando lo que estémos haciendo sea muy abstracto
  - Cuando se esté programando una regla muy peculiar
  - Cuando se explique un código difícil de optimizar
  - Cuando se explique un código difícil de leer

- Comentar siempre que sea necesario
- Ser conciso y directo
- Agregar `//TODO:` para tareas pendientes

## Variables

- Debe ser descriptiva
- Debe ser lo más corto posible
- El estándar de los nombres debe ser discutido con el equipo
- Ejemplos

```js

const isAdult; // boolean
const hasSpace; // boolean

const strName; // string
const quantity; // integet or decimal ?

```

## No hardcode

Evitar usar parámetros en duro
- Rutas de navegación
- Mensajes de error / éxito
- Rutas a APIs externas
- etc.

Benefícios
- Fácil de mantener
- Fácil de encontrar
- Evitas errores de tipeo

Ejemplo.

```js
  // path.constants.ts
  export const PATH_CLIENTS_ORDERS = 'clients/orders';
```

```js
  this.router.navigateByUrl(PATH_CLIENTS_ORDERS);
```

## DRY - Don't repear yourself

- Reutiliza tu código
- Crea funciones / componentes

## Nesting

- Evita indentar demasiado tu código
- Crea functiones / componentes

```html
<div>
  <div>
    <div>
      <div>
        <div>
        ...
```

```js
  for ...
    for ...
      for ...
  
  map ...
    map ...
      map  ...
```

## Arquitecture & structure

- Conversa con tu equipo sobre la arquitectura y estructura a manejar
- Siempre debes seguir lo establecido para poder mantener el órden en el proyecto
- Es fácil para mantener y escalar
- Básate en patrones de diseño

## Code reviews

- Ayuda a mantener la calidad del código
- Ayuda a identificar oportunidades de mejora
- Ayuda a detectar errores
- Se aprende del código revisado y de las observaciones

## Don't write all at once

- Es mejor tener varias tareas pequeñas en vez de tener una grande
- Es más fácil para hacer seguimiento
- Ayuda con las dependencias
- Es más fácil para revisar
- Es más fácil de entender
- Ayuda en concentarse en una tarea concreta

## Plugin vs DIY (Do it yourself)

Evalúa con tu quipo qué es mejor considerando lo siguiente
- Tiempo de desarrollo
- Peso
- Flexibilidad
- Seguridad
- Confiabilidad

* recuerda evitar versiones en alpha o beta

## Testing

- Ayudan a verificar la integridad del proyecto
- Ayuda a identificar si el cambio implementado impacta a otras partes
- Evaluar si es necesario
  - Tiempos cortos de entrega
  - No es necesario en Startups
  - Es necesario si se trabaja con data sensible (pagos)

## Automation
PROS
- Ahorra tiempo en deployments
- Elimina la repetición manual
- Elimina el error humano

CONS
- Puede tener una carga técnica

## KISS - Keep it simple, stupid

Siempre tratar de mantenerlo todo simple y entendible

# Personal / Professional

## Free time, hobbies
Es saludable siempre tener tiempo fuera de la compuadora
- Despeja la mente
- Desestresa
- Ayuda a ver un problema desde otro punto de vista

## Learn new things
Tratar de aprender nuevas tecnologías
- Frontend / backend - Ayuda a poder visualizar las tareas desde otros puntos de vista. Cómo puede entregar la data el back y cómo puede recibir la data el front
- Traer buenas prácticas de otros frameworks
- Qué nuevas soluciones existen

## Coding style, be flexible

- No todos tienen la misma forma de programar que tú
- Conversa con el equipo sobre los estándares a manejar en el proyecto
- Tener un estándar ayuda a que todos puedan entender el código

## Google it
Antes de preguntar búscalo en internet, páginas como starckoverflow te pueden dar la respuesta

## Always Ask
Nunca tengas miedo a preguntar. Es mejor que te tilden de preguntón a que se quejen que nunca preguntas y todo sale mal.

- Si no encuentras la respuesta en internet
- Si es relacionado a una implementación en el proyecto
- Si es alguna duda o dilema

## Share
Siempre trata de compartir lo aprendido
- No hay mejor satisfacción que compartir
- Mejora la comunicación e intacción en el equipo
- Todos aprenden

## Team work
"Tan fuerte como el eslabón más débil"
- La entrega del proyecto depende de todos
- Si una parte sale mal es responsabilidad del equipo

## Communication
- Mantener la comunicación activa
- Tener grupos de chat para consultas y debate
- Comunicación hacia tu equipo y hacia el cliente
- Siempre ser honestos y claros
- Manejo de habilidades blandas

## Tools

- GIT / SVN - Versionamiento de código
- Husky - Hooks para commits
- CommitLint - Verifica formato de los mensajes del 
commit
- Gitlab-CI - Integración continua
- Tickets / Dashboard - Manejo de tareas
- Netlify - Deplyment y preview
- Storybook - Catálogo de componentes
- Prettier - Autoformatear código
- EditorConfig - Configurar formato
- Zeplin - Visualizar diseños


